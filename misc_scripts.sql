SELECT LPU,n_sh_lpu, d_sh_lpu , count(lpu) from TEMP.check_tarif_policlinic_results_ GROUP BY LPU,n_sh_lpu, d_sh_lpu ;
SELECT DISTINCT LPU from check_tarif_policlinic_results;
CREATE TEMPORARY TABLE check_tarif_policlinic_results_ AS
  SELECT * from check_tarif_policlinic_results;
  
SELECT * FROM TEMP.check_tarif_policlinic_results_;
DELETE FROM TEMP.check_tarif_policlinic_results_ WHERE ratio in 
  ( SELECT ratio from TEMP.check_tarif_policlinic_results_ GROUP BY LPU,ratio HAVING COUNT(ratio)>1) ;
  
CREATE TABLE check_tarif_policlinic_results1 AS SELECT * FROM TEMP.check_tarif_policlinic_results_;

CREATE TABLE k20060_MEK AS
SELECT lpu, n_sh_lpu, d_sh_lpu, npp,npolis, ds, date_open, date_zak, deffectcodeid ,s_all,vp_oms FROM check_tarif_policlinic_results1
UNION
SELECT lpu, n_sh_lpu, d_sh_lpu, npp,npolis, ds, date_open, date_zak, deffectcodeid ,s_all,vp_oms FROM check_crossing_poiliclic_within_stac_results;

SELECT b.DEFFECTCODE,b.DEFFECTDESCRIPTION, sum(a.s_all) from k20060_MEK a  
LEFT JOIN deffects_basises b  
on a.deffectcodeid=b.id
GROUP BY a.deffectcodeid;