import sqlite3
from appy.pod.renderer import Renderer

def acclist(lpu):
    #получаем список счетов для указанного МО
    global cur
    cur.execute(f'SELECT N_SH_LPU, D_SH_LPU FROM check_tarif_policlinic_results WHERE LPU={lpu} GROUP BY N_SH_LPU, D_SH_LPU')
    return cur.fetchall()

def reclist(lpu, accnum, accdat):
    # получаем список мэковских позиций по счету от мо
    global cur
    cur.execute(f'''
        SELECT  LPU, N_SH_LPU, D_SH_LPU, VP_OMS, NPP, NPOLIS, DS, DATE_OPEN, DATE_ZAK, '7.7.7' as DEFECTCODE, 'ОПИСАНИЕ ОШИБКИ' as DEFECTDESCR , S_ALL
        FROM check_tarif_policlinic_results WHERE LPU='{lpu}' AND N_SH_LPU='{accnum}' ''')
    return cur.fetchall()

def mec_statistics_list(basis):
   global mec_statistics
   return [x for x in mec_statistics if x[0]==basis]


db = sqlite3.connect('remek_kap_2006.db')
cur = db.cursor()

#считаем общую статистику
cur.execute('''
    SELECT ( CASE
                 WHEN vp_oms='1' THEN 'в стационаре:'
	             WHEN vp_oms='2' THEN 'в дн. стационаре:'
	             WHEN vp_oms='3' THEN 'в амбулатории:'
	             WHEN vp_oms='4' THEN 'скорая мед. помощь:'
	         END
           ) AS vp_oms, count(VP_OMS), round(sum(S_ALL),2)
    FROM k20060
    GROUP BY vp_oms''')
gen_statistics = cur.fetchall()

cur.execute('SELECT COUNT(DISTINCT LPU) FROM k20060')
num_mo = cur.fetchone()[0]

cur.execute('SELECT DISTINCT LPU,N_SH_LPU,D_SH_LPU FROM k20060')
reestrs = cur.fetchall()
num_ree = len(reestrs)

#считаем общую статистику для секции статистики по МЭК'ам
cur.execute('SELECT COUNT(lpu), SUM(S_ALL) FROM k20060_MEK ')
mec_gen_statistic = cur.fetchone()
#считаем статистику для секции статистики по МЭКам в разрезе кодов и условий оказания
cur.execute('''
    SELECT b.DEFFECTCODE,b.DEFFECTDESCRIPTION, 
    ( CASE
                 WHEN a.vp_oms='1' THEN 'в стационаре:'
	             WHEN a.vp_oms='2' THEN 'в дн. стационаре:'
	             WHEN a.vp_oms='3' THEN 'в амбулатории:'
	             WHEN a.vp_oms='4' THEN 'скорая мед. помощь:'
	         END
           ) AS vp_omsa, count(a.s_all), sum(a.s_all) from k20060_MEK a  
    LEFT JOIN deffects_basises b  
    ON a.deffectcodeid=b.id
    GROUP BY a.deffectcodeid, a.vp_oms''')
mec_statistics = cur.fetchall()

#готовим данные для блока экспертиз по МО по счетам
cur.execute('''
    SELECT DISTINCT a.LPU, b.SNAME, a.S_ALL , a.P, a.S, a.Z, a.A
    FROM (
          SELECT lpu, round(sum(S_ALL),2) s_all,
       COALESCE(sum(CASE WHEN vp_oms='3' THEN s_all END), 0) P,
	   COALESCE(sum(CASE WHEN vp_oms='1' THEN s_all END), 0) S,
	   COALESCE(sum(CASE WHEN vp_oms='2' THEN s_all END), 0) Z,
	   COALESCE(sum(CASE WHEN vp_oms='4' THEN s_all END), 0) A
       FROM k20060_MEK
       GROUP BY lpu) a
    LEFT JOIN F003 b
    ON a.LPU=b.LPU''')
molist=cur.fetchall()

#cur.execute('''
#    SELECT  LPU, N_H_LPU, D_SH_LPU, VP_OMS, NPP, NPOLIS, DS, DATE_OPEN, DATE_ZAK, '7.7.7' as DEFECTCODE, 'ОПИСАНИЕ ОШИБКИ' as DEFECTDESCR , SA_LL
#    FROM check_tarif_policlinic_results
#    ORDER BY LPU, N_SH_LPU, D_SH_LPU ''')


# собираем отчет
renderer = Renderer('firsttry.odt', globals(), 'aktI2010.odt')
renderer.run()
